// Real-time Busu Tracking System
// Author: Lessandro Z. Mariano

var day = 60*60*24;
var hour = 60*60;
var minute = 60;

var speed = 20 * 1000 / hour;

var map, ds;
var routes = [];
var info, markers = {};

// ---------

function image(url, w, h, x, y, sx, sy) {
  return new google.maps.MarkerImage(url,
    new google.maps.Size(w, h),
    new google.maps.Point(0, 0),
    new google.maps.Point(x, y || h),
    new google.maps.Size(sx || w, sy || h))
};

var icons = {
  'bus': {
    icon: "icons/small_#.png",
    shadow: image("icons/small_shadow.png", 22, 20, 6)
  },
  'stop': {
    icon: image("icons/stop.png", 11, 11, 5, 5, 9, 9)
  },
  'main': {
    icon: "icons/big_green.png",
    shadow: image("icons/big_shadow.png", 37, 34, 10)
  }
}

function addmarker(type, pos, name, tag, icon, shadow) {
  tag = tag || type+"#"+pos;

  if (markers[tag]) {
    markers[tag].setPosition(pos);
  } else {
    var marker = markers[tag] = new google.maps.Marker({
      map: map,
      position: pos,
      icon: icon || icons[type].icon,
      shadow: shadow || icons[type].shadow
    });

    marker.name = name;

    google.maps.event.addListener(marker, 'click', function() {
      if (info) {
        info.close();
      }
      info = new google.maps.InfoWindow({
        content: queryinfo(tag)
      });
      info.open(map, marker);
    });
  }

  return tag;
}

function addbus(pos, name, time, color) {
  tag = "bus#"+name+"#"+time;
  icon = icons.bus.icon.replace("#", color);
  addmarker("bus", pos, name, tag, icon);
}

function queryinfo(tag) {
  return $new("div").add([tag,{br:{},b:markers[tag].name}]);
}

// ---------

var colors = ["yellow","white","black","orange",
  "purple","brown","blue","red","green"];

function nextcolor() {
  var color = colors.pop();
  colors.unshift(color);
  return color;
}

function latlng(pos){
  return new google.maps.LatLng(pos[0], pos[1]);
}

function Route(route) {
  this.name = route.name;
  this.table = route.table;
  this.color = nextcolor();

  _(route.name+" ["+this.color+"]");

  this.origin = latlng(route.origin);
  this.destination = latlng(route.destination);

  this.path = [];
  this.path.push(addmarker('main', this.origin, route.origin[2]));

  this.waypoints = [];
  route.waypoints.each(function(pos) {
    this.waypoints.push({location: latlng(pos)});
    if (pos[2]) {
      this.path.push(addmarker('stop', latlng(pos), pos[2]));
    }
  }, this);

  this.path.push(addmarker('main', this.destination, route.destination[2]));
}

function calcdists(path, realdist) {
  var a = path.clone(); a.pop();
  var b = path.clone(); b.shift();

  dists = a.zip(b, function(x){ return distance(x[0], x[1]) });
  dist = dists.sum();
  return dists.map(function(x){ return x*realdist/dist });
}

Route.prototype.process = function(response){
  this.routes = [];

  response.routes[0].legs.each(function(r){
    route = {
      path: r.steps.pluck("lat_lngs").flatten(),
      distance: r.distance.value,
      duration: r.distance.value / speed,
      dists: null
    };
    route.dists = calcdists(route.path, route.distance);
    this.routes.push(route);
  }, this);

  this.distance = this.routes.pluck("distance").sum();
  this.duration = this.routes.pluck("duration").sum();

  var poly = new google.maps.Polyline({
    path: this.routes.pluck("path").flatten(),
    strokeColor: this.color,
    strokeOpacity: 0.7,
    strokeWeight: 3
  });
  poly.setMap(map);
};

Route.prototype.whereisit = function(secs) {
  var remaining = secs * speed; // distance remaining, meters

  for (var i=0; i<this.routes.length; i++) {
    var r = this.routes[i];

    if (remaining > r.distance) {
      remaining -= r.distance;
      continue;
    }

    for (var j=0; j<r.dists.length; j++) {
      if (remaining > r.dists[j]) {
        remaining -= r.dists[j];
        continue;
      }

      return midvertex(r.path[j], r.path[j+1], remaining/r.dists[j]);
    }
  }

  return null;
}

// --------

function midvertex(v1, v2, ratio) {
  var lat = v1.lat() + (v2.lat()-v1.lat())*ratio;
  var lng = v1.lng() + (v2.lng()-v1.lng())*ratio;
  return new google.maps.LatLng(lat, lng);
}

function distance(v1, v2) {
  var dy = v2.lat()-v1.lat();
  var dx = v2.lng()-v1.lng();
  return Math.sqrt(dx*dx+dy*dy);
}

function timewarp() {
  routes.each(function(route) {
    var d = new Date();
    var now = d.getHours()*hour+d.getMinutes()*minute+d.getSeconds();
    var bottom = now - route.duration;

    route.table.each(function(secs) {
      if (bottom < secs && secs < now) {
        position = route.whereisit(now-secs);
        if (position)
          addbus(position, route.name, secs, route.color);
      }
    });
  });
}

// --------

var stack = [];

function postload() {
  setInterval("timewarp()", 1);
}

function initialize() {
  var opts = {
    zoom: 12,
    center: new google.maps.LatLng(-20.311970,-40.317507),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  };

  map = new google.maps.Map($("map"), opts);
  ds = new google.maps.DirectionsService();

  stack = _routes;
  next_route();
}

function next_route() {
  if (!stack.length) {
    postload();
    return
  }

  var route = new Route(stack.pop());
  routes.push(route);

  var request = {
      origin: route.origin,
      destination: route.destination,
      waypoints: route.waypoints,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
  };

  ds.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      routes.last().process(response);
    } else {
      _("ds response: "+status);
    }
    next_route();
  });
}

// ------

function _(s) { $("_").add(s+'\n') }

function $new(t) { return document.createElement(t); }

Array.prototype.sum = function(c) {
  var ret = 0;
  this.each(function(value){ ret += value; });
  return ret;
}

Element.prototype.add = function(c) {
  parse(c).each(this.appendChild, this);
  return this;
}

function parse(o) {
  var ret = [];
  if (typeof o == "string") return [document.createTextNode(o)];
  if (o instanceof Array) o.each(function(e){ ret.push(parse(e)) });
  else for (var k in o) ret.push($new(k).add(o[k]));
  return ret.flatten();
}

// $("t").add({b:"bbb",br:{},i:["asd",{br:{}},"ccc"]});
