var stops = {
  cariacica: {
    t_campo_grande: [-20.344697,-40.397812, "Terminal de Campo Grande"],
    br_autopecas: [-20.339979,-40.39499, "BR - Autopeças"],
    exp_garcia_1: [-20.341081,-40.390136, "Av. Exp. Garcia - 1"],
    exp_garcia_2: [-20.341227,-40.383618, "Av. Exp. Garcia - 2"],
    pio_xii: [-20.338269,-40.384648, "Faculdade Pio XII"]
  },

  vitoria: {
    segunda_ponte: [-20.323702,-40.354307],
    rodoviaria: [-20.321015,-40.350863, "Rodoviária"],
    rodoviaria_2: [-20.319758,-40.350702, "Rodoviária - 2"],
    h_novaes: [-20.318963,-40.329748],
    r_aristeu: [-20.32019,-40.329105],
    salesiano: [-20.317162,-40.324352, "Salesiano"],
    cefetes: [-20.312031,-40.3174, "CEFET-ES"],
    boulevard: [-20.307825,-40.296328, "Shopping Boulevard"],
    ufes_passarela: [-20.277656,-40.301585, "UFES - Passarela"],
    sh_vitoria: [-20.312594,-40.289698, "Shopping Vitória"],
    jd_camburi: [-20.263747,-40.263798, "Jardim Camburi"]
  },

  serra: {
    sao_domingos: [-20.231778,-40.271394],
    t_carapina: [-20.231214,-40.271105, "Terminal de Carapina"],
    t_laranjeiras: [-20.195422,-40.257125, "Terminal de Laranjeiras"],
    elesbao: [-20.122715,-40.315447, "R. Elesbão Alexandre Miranda"],
    jones: [-20.129284,-40.304503, "Av. Jones dos Santos Neves"],
    garagem: [-20.116590,-40.316133, "Garagem Serrana"]
  },

  vilavelha: {
  },

  viana: {
  }
}

var _routes = [
  {
    name: "509: Carapina -> Campo Grande",

    origin: stops.serra.t_carapina,
    destination: stops.cariacica.t_campo_grande,

    waypoints: [
      stops.vitoria.ufes_passarela,
      stops.vitoria.boulevard,
      //stops.vitoria.cefetes,
      //stops.vitoria.salesiano,
      stops.vitoria.h_novaes,
      stops.vitoria.rodoviaria_2,
      stops.cariacica.pio_xii,
      stops.cariacica.exp_garcia_2,
      stops.cariacica.exp_garcia_1,
      stops.cariacica.br_autopecas,
    ],

    table: [17100,18000,18720,19440,20160,20880,21600,22320,23040,23760,24480,25200,25920,26640,27360,28200,28800,29520,30240,30960,31680,32400,33000,33600,34200,34800,35400,36000,36600,37200,37800,38400,39000,39600,40200,40800,41400,42000,42600,43200,43800,44400,45000,45600,46200,46800,47400,48000,48600,49200,49740,50280,50820,51360,51900,52440,52980,53520,54060,54600,55200,55800,56400,57000,57600,58320,59040,59760,60480,61200,61920,62640,63600,64800,66000,67200,68400,69600,70800,72000,73200,74400,75600,76800,78000,79200,80400,81600,82800,84300]
  },
  {
    name: "509: Campo Grande -> Carapina",

    origin: stops.cariacica.t_campo_grande,
    destination: stops.serra.t_carapina,

    waypoints: [
      stops.cariacica.br_autopecas,
      stops.cariacica.exp_garcia_1,
      stops.cariacica.exp_garcia_2,
      stops.vitoria.rodoviaria,
      stops.vitoria.salesiano,
      //stops.vitoria.cefetes,
      stops.vitoria.boulevard,
      stops.vitoria.ufes_passarela,
      stops.serra.sao_domingos
    ],

    table: [18000,18960,19920,20880,21600,22140,22680,23220,23760,24300,24840,25380,25920,26460,27000,27540,28080,28620,29160,29700,30240,30960,31680,32400,33000,33600,34200,34800,35400,36000,36600,37200,37800,38400,39000,39600,40200,40800,41400,42000,42600,43200,43800,44400,44940,45480,46020,46560,47100,47700,48300,48900,49500,50100,50700,51300,51900,52500,53100,53700,54300,54900,55500,56100,56700,57300,57900,58500,59400,60300,61200,62100,63000,63900,64800,65700,66600,67500,68400,69300,70200,71100,72000,73200,74400,75600,76500,77400,79500,81300,83700,0]
  },
  {
    name: "591: Serra -> Campo Grande",

    origin: stops.serra.garagem,
    destination: stops.cariacica.t_campo_grande,

    waypoints: [
      stops.serra.elesbao,
      stops.serra.jones,
      stops.vitoria.ufes_passarela,
      stops.vitoria.boulevard,
      //stops.vitoria.cefetes,
      stops.vitoria.salesiano,
      stops.vitoria.h_novaes,
      stops.vitoria.rodoviaria_2,
      stops.cariacica.br_autopecas,
    ],

    table: [14400,15600,16800,17700,18180,18660,19080,19680,20580,21780,22740,24660,25140,26340,26940,27720,28140,28560,29040,29640,30240,30960,31680,32400,33120,33840,34560,35280,36000,36720,37440,38160,38880,39600,40200,40800,41400,42000,42600,43200,43800,44400,45000,45600,46200,46800,47460,48120,48780,49380,50040,50640,51300,51900,52560,53160,53820,54420,55080,55680,56340,57000,57660,58200,58980,59700,60240,60720,61200,61680,62160,62640,63120,63600,64080,64800,65520,65880,66600,67560,68040,69000,70200,70800,71400,72000,72600,73200,73800,74400,75000,75600,76320,77040,78000,79500,81000,83100,85200]
  },
  {
    name: "591: Campo Grande -> Serra",

    origin: stops.cariacica.t_campo_grande,
    destination: stops.serra.garagem,

    waypoints: [
      stops.cariacica.br_autopecas,
      stops.vitoria.rodoviaria,
      stops.vitoria.salesiano,
      stops.vitoria.cefetes,
      stops.vitoria.boulevard,
      stops.vitoria.ufes_passarela,
      stops.serra.jones,
      stops.serra.elesbao,
    ],

    table: [18000,18600,19200,19860,20760,21240,21720,22200,22680,23160,23640,24120,24600,25080,25560,26280,26760,27300,27900,28500,29100,29700,30420,30960,31680,32400,33120,33840,34560,35280,36000,36900,37800,38700,39600,40320,41040,41760,42480,43200,43920,44640,45360,46080,46800,47520,48240,48960,49680,50400,51000,51600,52200,52800,53400,54000,54720,55440,56160,56880,57360,58140,58860,59580,60300,61020,61740,62460,62820,63180,63900,64620,65340,66060,66780,67500,68220,68940,69480,70020,70560,71100,71700,72300,72900,73500,74100,74700,75300,75900,76500,77100,77700,78300,79200,79800,80400,81300,82500,84300,0]
  },
  {
    name: "515: Laranjeiras -> Campo Grande",

    origin: stops.serra.t_laranjeiras,
    destination: stops.cariacica.t_campo_grande,

    waypoints: [
      stops.serra.t_carapina,
      stops.vitoria.jd_camburi,
      stops.vitoria.sh_vitoria,
      stops.vitoria.r_aristeu,
      stops.vitoria.h_novaes,
      stops.vitoria.rodoviaria_2,
      stops.cariacica.br_autopecas
    ],

    table: [18000,19200,20400,21600,23400,25200,26400,27900,29340,30420,31500,32580,33540,34500,35400,36300,37200,38100,39000,39900,40680,41460,42240,43020,43800,44700,45600,46500,47400,48300,49200,50100,51000,51900,52800,53700,54600,55500,56400,57180,57840,58320,58800,59280,59760,60240,60720,61140,61560,61980,62400,62880,63420,63960,64560,65160,65820,66480,67140,67740,68400,69300,70200,71100,72000,72720,73440,74160,74880,75600,76500,77400,78300,79200,81000,82080,83400]
  },
  {
    name: "515: Campo Grande -> Laranjeiras",

    origin: stops.cariacica.t_campo_grande,
    destination: stops.serra.t_laranjeiras,

    waypoints: [
      stops.cariacica.br_autopecas,
      stops.vitoria.rodoviaria,
      stops.vitoria.sh_vitoria,
      stops.vitoria.jd_camburi,
      stops.serra.t_carapina
    ],

    table: [17100,17700,18180,18600,18960,19260,19560,19860,20160,20460,20700,20940,21180,21420,21720,21960,22260,22500,22800,23040,23340,23580,23820,24060,24300,24540,24840,25080,25320,25620,25860,26100,26400,26640,26940,27240,27540,27840,28140,28440,28740,29100,29460,29880,30300,30900,31500,32100,32700,33300,34200,35100,36000,36900,37800,38700,39600,40500,41400,42300,43200,44100,45000,45900,46800,47580,48360,49140,49860,50580,51300,52020,52740,53460,54180,54900,55800,56700,57600,58500,59400,60300,61200,61920,62640,63360,64080,64800,65520,66240,66960,68100,69300,70500,72000,73500,75000,76500,77880,79200,80520,81960,83400]
  }
];
